/**
 * InfoScreen is a private Software. 
 * Without a written permission from Antonio Mark (anux.linux@gmail.com) 
 * you are not allowed to change the programm.
 */
package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import main.MainThread;

/**
 * @author Antonio
 * 
 */
public class InfoPane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8823082398881371212L;

	private final String userDir = System.getProperty("user.dir");
	private final String fileSep = System.getProperty("file.separator");

	private Font infoFont = new Font("Calibri", Font.TRUETYPE_FONT, 300);
	private final Font errorFont = new Font("Calibri", Font.TRUETYPE_FONT, 20);
	private final int defaultOffset = 100;
	private final int slidingOffset = 100;
	private final int visibleOffset = 100;
	private boolean secoundRow = false;

	private ArrayList<String> currentLines = new ArrayList<String>();
	private ArrayList<Integer> currentXPos = new ArrayList<Integer>();
	private int currentRow = 0;
	private boolean update = true;

	public InfoPane() {

		// loadHtml();
		// this.setEditorKit(new HTMLEditorKit());
	}

	@Override
	public void paintComponent(Graphics g) {
		try {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, getWidth(), getHeight());

			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());

			g.setColor(Color.WHITE);
			if (getHeight() < 300) {
				infoFont = new Font("Calibri", Font.TRUETYPE_FONT, getHeight());
			}

			g.setFont(infoFont);

			// g.drawLine(0, 384, getWidth(), 384);

			paintText(g);
		} catch (Exception e) {
			e.printStackTrace();
			g.setFont(errorFont);
			g.drawString("error Occured", 20, 20);
		}
	}

	private void paintText(Graphics g) {
		ArrayList<String> loadedLines;
		try {
			loadedLines = loadLines();
			if (update) {
				currentLines = loadedLines;
				currentXPos = new ArrayList<Integer>(Collections.nCopies(currentLines.size(), defaultOffset));
			}

			// if (text.length == 2) {
			// paintImage(text[1], g);

			// } else {

			FontMetrics metrics = g.getFontMetrics(infoFont);

			if (currentLines.size() == 1) {
				// There is only one line
				String text = currentLines.get(0);

				// get metrics from the graphics
				int textWidth = metrics.stringWidth(text);

				int xpos = getWidth() - defaultOffset;

				if (getWidth() < textWidth) {
					// The text is wider than the screen. Make a sliding Text.
					xpos = currentXPos.get(0);
					int newxpos = xpos - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= textWidth) {
						currentXPos.set(0, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(0, newxpos);
					}

					// System.out.println(xpos);

				} else {
					xpos = (getWidth() - metrics.stringWidth(text)) / 2;
				}

				int ypos = metrics.getAscent() + (getHeight() - (metrics.getAscent() + metrics.getDescent())) / 2;
				drawString(g, text, xpos, ypos);
			} else if (currentLines.size() == 2) {
				// There are two lines. Display them in one line each.

				String text1 = currentLines.get(0);
				String text2 = currentLines.get(1);

				int center = getHeight() / 2;
				int ypos = metrics.getAscent() + 20;

				// First line.
				int textWidth1 = metrics.stringWidth(text1);
				int xpos1 = 0;
				if (getWidth() < textWidth1 && !secoundRow) {
					// The text is wider than the screen. Make a sliding Text.
					xpos1 = currentXPos.get(0);
					int newxpos = xpos1 - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= textWidth1) {
						currentXPos.set(0, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(0, newxpos);
					}

					// System.out.println(xpos);

				} else if (getWidth() < textWidth1 && secoundRow) {
					xpos1 = defaultOffset;
				} else {
					int pos = currentXPos.get(0);
					int newxpos = pos - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= getWidth()) {
						currentXPos.set(0, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(0, newxpos);
					}

					xpos1 = (getWidth() - metrics.stringWidth(text1)) / 2;
				}

				drawString(g, text1, xpos1, ypos);

				// Second line
				int textWidth2 = metrics.stringWidth(text2);
				int xpos2 = 0;

				if (getWidth() < textWidth2 && secoundRow) {
					// The text is wider than the screen. Make a sliding Text.
					xpos2 = currentXPos.get(1);
					int newxpos = xpos2 - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= textWidth2) {
						currentXPos.set(1, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(1, newxpos);
					}

					// System.out.println(xpos);

				} else if (getWidth() < textWidth2 && !secoundRow) {
					xpos2 = defaultOffset;
				} else {
					int pos = currentXPos.get(1);
					int newxpos = pos - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= getWidth()) {
						currentXPos.set(1, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(1, newxpos);
					}

					xpos2 = (getWidth() - metrics.stringWidth(text2)) / 2;
				}

				ypos = center + metrics.getAscent() + 20;

				drawString(g, text2, xpos2, ypos);

				if (currentXPos.get(0) == (getWidth() - visibleOffset)) {
					currentXPos.set(0, defaultOffset);
					secoundRow = true;
				}
				if (currentXPos.get(1) == (getWidth() - visibleOffset)) {
					currentXPos.set(1, defaultOffset);
					secoundRow = false;
				}

			} else {
				// There are more than two lines. Make a rolling text.

				// Get the first Line and the next Line
				int numberLines = currentLines.size();
				int nextRow = (currentRow + 1) % numberLines;
				String text1 = currentLines.get(currentRow);
				String text2 = currentLines.get(nextRow);

				int center = getHeight() / 2;
				int ypos = metrics.getAscent() + 20;

				// First line.
				int textWidth1 = metrics.stringWidth(text1);
				int xpos1 = 0;
				if (getWidth() < textWidth1) {
					// The text is wider than the screen. Make a sliding Text.
					xpos1 = currentXPos.get(currentRow);
					int newxpos = xpos1 - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= textWidth1) {
						currentXPos.set(currentRow, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(currentRow, newxpos);
					}

					// System.out.println(xpos);

				} else {
					int pos = currentXPos.get(currentRow);
					int newxpos = pos - slidingOffset;
					// int visibleTextWidth = (textWidth - getWidth());
					if (Math.abs(newxpos) >= getWidth()) {
						currentXPos.set(currentRow, getWidth() - visibleOffset);
					} else {
						// System.out.println("Set to newx");
						currentXPos.set(currentRow, newxpos);
					}

					xpos1 = (getWidth() - metrics.stringWidth(text1)) / 2;
				}

				drawString(g, text1, xpos1, ypos);

				// Second line
				int textWidth2 = metrics.stringWidth(text2);
				int xpos2 = 0;
				if (getWidth() < textWidth2) {
					// The text is wider than the screen. Make a sliding Text.
					xpos2 = defaultOffset;
				} else {
					xpos2 = (getWidth() - metrics.stringWidth(text2)) / 2;
				}

				ypos = center + metrics.getAscent() + 20;

				drawString(g, text2, xpos2, ypos);

				if (currentXPos.get(currentRow) == (getWidth() - visibleOffset)) {
					currentXPos.set(currentRow, defaultOffset);
					currentRow = nextRow;
				}
			}

			// }

			update = false;

		} catch (IOException e) {
			MainThread.logger.log(Level.WARNING, e.getMessage(), e);
		}

	}

	private void drawString(Graphics g, String s, int xpos, int ypos) {
		g.drawString(s, xpos, ypos);
	}

	private void paintImage(String path, Graphics g) throws IOException {

		URL url = new URL(path);
		URLConnection conn = url.openConnection();
		InputStream in = conn.getInputStream();

		BufferedImage img = null;

		img = ImageIO.read(in);

		g.drawImage(img, 0, 0, null);

	}

	private ArrayList<String> loadLines() throws IOException {

		// String textFile = userDir + fileSep + "res" + fileSep +
		// "infoText.html";
		//
		// File f = new File(textFile);
		//
		// URL url = f.toURI().toURL();

		URL url = new URL("http://localhost/monitor.txt");

		URLConnection spoof = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(spoof.getInputStream(), "UTF-8"));
		String strLine = "";
		ArrayList<String> newElements = new ArrayList<String>();

		int lines = 0;

		// Loop through every line in the source
		while ((strLine = in.readLine()) != null) {

			if (strLine.startsWith("img:") && lines == 0) {
				MainThread.logger.log(Level.WARNING, "Displaying Image not supported yet!");
				update = false;
				return currentLines;
			} else {
				String[] tempText = strLine.split(":", 2);
				if (!update) {
					try {
						String oldElement = currentLines.get(lines);
						if (!oldElement.equals(tempText[1])) {
							update = true;
						}
					} catch (IndexOutOfBoundsException e) {
						update = true;
					}
				}
				newElements.add(tempText[1]);
				lines++;
			}

		}
		System.out.println(newElements.size());
		System.out.println(currentLines.size());
		if (newElements.size() != currentLines.size()) {
			update = true;
		}
		return newElements;
	}
}

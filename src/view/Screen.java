/**
 * InfoScreen is a private Software. 
 * Without a written permission from Antonio Mark (anux.linux@gmail.com) 
 * you are not allowed to change the programm.
 */
package view;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

/**
 * @author Antonio
 * 
 */
public class Screen {

	private final JPanel ta = new InfoPane();

	public Screen() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		generateFrame();
	}

	private void generateFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		final JFrame screenSaverFrame = new JFrame();
		screenSaverFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		screenSaverFrame.setUndecorated(true);
		screenSaverFrame.setResizable(false);

		screenSaverFrame.setLayout(new BorderLayout());
		screenSaverFrame.getContentPane().add(ta);

		screenSaverFrame.validate();
		GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
				.setFullScreenWindow(screenSaverFrame);
		// screenSaverFrame.pack();
		// screenSaverFrame.setVisible(true);

		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");

		// Set the blank cursor to the JFrame.
		screenSaverFrame.getContentPane().setCursor(blankCursor);
	}

	public void updateScreen() {
		ta.repaint();
	}
}

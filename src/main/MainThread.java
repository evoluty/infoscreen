/**
 * InfoScreen is a private Software. 
 * Without a written permission from Antonio Mark (anux.linux@gmail.com) 
 * you are not allowed to change the programm.
 */
package main;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import view.Screen;

/**
 * @author Antonio
 * 
 */
public class MainThread extends Thread {

	public final static Logger logger = Logger.getLogger("MainThread");
	private final Screen screen;

	public MainThread(Screen screen) throws SecurityException, IOException {
		super();
		configLogger();
		this.screen = screen;

	}

	private void configLogger() throws SecurityException, IOException {
		// This block configure the logger with handler and formatter
		File logfiel = new File(new File(System.getProperty("user.dir")), "Screen.log");
		FileHandler fh = new FileHandler(logfiel.getAbsolutePath());
		MainThread.logger.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);

		MainThread.logger.setLevel(Level.WARNING);
	}

	@Override
	public void run() {
		try {
			synchronized (this) {
				while (true) {
					screen.updateScreen();
					simulateActivity();
					this.wait(1000);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// System.exit(0);
		}

	}

	private void simulateActivity() {
		try {
			Robot robot = new Robot();

			// Simulate a key press
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);

		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
}

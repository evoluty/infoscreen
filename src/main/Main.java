/**
 * InfoScreen is a private Software. 
 * Without a written permission from Antonio Mark (anux.linux@gmail.com) 
 * you are not allowed to change the program.
 */
package main;

import java.io.IOException;

import javax.swing.UnsupportedLookAndFeelException;

import view.Screen;

/**
 * @author Antonio
 * 
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MainThread mainthread;
		try {
			Screen s = new Screen();
			mainthread = new MainThread(s);
			mainthread.start();
		} catch (SecurityException e) {
			System.out.println("Error Starting Programm");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error Starting Programm");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("Error Starting Programm");
			e.printStackTrace();
		} catch (InstantiationException e) {
			System.out.println("Error Starting Programm");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.out.println("Error Starting Programm");
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			System.out.println("Error Starting Programm");
			e.printStackTrace();
		}

	}

}
